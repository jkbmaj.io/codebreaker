<?php

namespace App\Service;

class StringEncDecoder
{
    private const SIGN_MAP = [
        '!' => 'a',
        ')' => 'b',
        '"' => 'c',
        '(' => 'd',
        '£' => 'e',
        '*' => 'f',
        '%' => 'g',
        '&' => 'h',
        '>' => 'i',
        '<' => 'j',
        '@' => 'k',
        'a' => 'l',
        'b' => 'm',
        'c' => 'n',
        'd' => 'o',
        'e' => 'p',
        'f' => 'q',
        'g' => 'r',
        'h' => 's',
        'i' => 't',
        'j' => 'u',
        'k' => 'v',
        'l' => 'w',
        'm' => 'x',
        'n' => 'y',
        'o' => 'z',
    ];

    public function decode(string $encPhrase): string
    {
        return $this->convert($encPhrase, self::SIGN_MAP);
    }

    public function encode(string $phrase): string
    {
        $invertedMap = array_flip(self::SIGN_MAP);

        return $this->convert($phrase, $invertedMap);
    }

    private function convert($string, $signMap): string
    {
        $stringArray = mb_str_split($string);

        $convertedString = [];
        foreach ($stringArray as $letter) {
            $convertedString[] = (isset($signMap[$letter])) ? $signMap[$letter] : $letter;
        }

        return implode('', $convertedString);
    }
}
