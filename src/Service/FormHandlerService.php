<?php

namespace App\Service;

class FormHandlerService
{
    private const DECODE = 'decode';
    private const ENCODE = 'encode';

    public function __construct(private readonly StringEncDecoder $codeBreakerService)
    {
    }

    public function formHandler(): string
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return 'Nieprawidłowa akcja';
        }

        $selectedMode = $_POST['mode'];
        $phrase = $_POST['phrase'];

        return match ($selectedMode) {
            self::DECODE => $this->codeBreakerService->decode($phrase),
            self::ENCODE => $this->codeBreakerService->encode($phrase),
            default => 'Wybierz tryb działania',
        };
    }
}
