<?php

namespace App\Tests\Unit\Service;

use App\Service\StringEncDecoder;
use PHPUnit\Framework\TestCase;

class CodeBreakerServiceTest extends TestCase
{
    private StringEncDecoder $codeBreakerService;

    protected function setUp(): void
    {
        $this->codeBreakerService = new StringEncDecoder();
    }

    /**
     * @covers StringEncDecoder::decode()
     */
    public function testDecodePhrase(): void
    {
        $actual = $this->codeBreakerService->decode(')g!ld, j(!ad "> h>ę gdol>!o!" o!(!c>£');

        $this->assertEquals('brawo, udalo ci się rozwiazac zadanie', $actual);
    }

    /**
     * @covers StringEncDecoder::encode()
     */
    public function testEncodePhrase(): void
    {
        $actual = $this->codeBreakerService->encode('Zażółć, gęślą jaźń');

        $this->assertEquals('Z!żółć, %ęśaą <!źń', $actual);
    }
}